<?php
    $baseUrl = "/";
?>
<!doctype html>
<html lang="lv">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Douglas - banner</title>
    <link href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One" rel="stylesheet">
    <link href="<?php echo $baseUrl; ?>css/screen.css?v3" rel="stylesheet">



    
</head>

<body style="background-color: #b6dccb; margin: 0 0 0 0; padding: 0;">




    <div class="responsive-example" style="max-width: 960px; margin: 100px auto 0 auto;">
    
        <div class="Xmas2016 Narrow WithoutButton">
            <img src="<?php echo $baseUrl; ?>images/presents.png" class="Xmas2016__Presents Xmas2016__Presents--Left" alt="" />
            <img src="<?php echo $baseUrl; ?>images/presents.png" class="Xmas2016__Presents Xmas2016__Presents--Right" alt="" />
            <div class="Xmas2016__Content">
                <div class="Xmas2016__Text">Līdz 2017. gadam -</div>
                <div id="countdown" class="Xmas2016__Clock">
                    

                    <div class="Xmas2016__DigitGroup">
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-day-1">
                            0
                        </div>
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-day-2">
                            0
                        </div>
                        <div class="Xmas2016__Label">DIENAS</div>
                    </div><div class="Xmas2016__Divider">:</div><div class="Xmas2016__DigitGroup">
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-hour-1">
                            0
                        </div>
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-hour-2">
                            0
                        </div>
                        <div class="Xmas2016__Label">STUNDAS</div>
                    </div><div class="Xmas2016__Divider">:</div><div class="Xmas2016__DigitGroup">
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-minute-1">
                            0
                        </div>
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-minute-2">
                            0
                        </div><div class="Xmas2016__Label">MINŪTES</div>
                    </div><div class="Xmas2016__Divider">:</div><div class="Xmas2016__DigitGroup">
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-second-1">
                            0
                        </div>
                        <div class="Xmas2016__Digit Xmas2016__Digit--first js-second-2">
                            0
                        </div>
                        <div class="Xmas2016__Label">SEKUNDES</div>
                    </div>
                </div>
            </div>

        </div>

        <div class="Xmas2016Landing">
            <a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/davanas-vinai/" class="Xmas2016Landing__Item">
                <img src="<?php echo $baseUrl; ?>images/landing/banner-her.jpg" alt="Dāvanas viņai">
            </a>

            <a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/davanas-vinam/" class="Xmas2016Landing__Item">
                <img src="<?php echo $baseUrl; ?>images/landing/banner-him.jpg" alt="Dāvanas viņam">
            </a>

            <a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/davana-berniem/" class="Xmas2016Landing__Item">
                <img src="<?php echo $baseUrl; ?>images/landing/banner-kids.jpg" alt="Dāvanas bērniem">
            </a>

            <a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/ekskluzivas-davanas/" class="Xmas2016Landing__Item">
                <img src="<?php echo $baseUrl; ?>images/landing/banner-exclusive.jpg" alt="Eksluzīvas dāvanas">
            </a>

        </div>

    </div>





<div id="snowflakeContainer">
    <img src="<?php echo $baseUrl; ?>images/snowflake.png" alt="" class="snowflake">
</div>


    <script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>

    <script src="<?php echo $baseUrl; ?>js/jquery.countdown.js"></script>
    <script src="<?php echo $baseUrl; ?>js/global.js"></script>
    
    <script src="<?php echo $baseUrl; ?>js/snow.js?v2"></script>
    
</body>

</html>


