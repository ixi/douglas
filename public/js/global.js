$(function() {
	$('#countdown').countdown('2016/12/31  23:59:59', function(event) {

		$('.js-second-1').html(String(event.strftime('%S')).charAt(0));
		$('.js-second-2').html(String(event.strftime('%S')).charAt(1));
		$('.js-minute-1').html(String(event.strftime('%M')).charAt(0));
		$('.js-minute-2').html(String(event.strftime('%M')).charAt(1));
		$('.js-hour-1').html(String(event.strftime('%H')).charAt(0));
		$('.js-hour-2').html(String(event.strftime('%H')).charAt(1));
		$('.js-day-1').html(String(event.strftime('%D')).charAt(0));
		$('.js-day-2').html(String(event.strftime('%D')).charAt(1));

	});

});