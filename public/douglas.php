<?php
    $baseUrl = "/";
?>
<!DOCTYPE html>
<html>
<head>
	<title> landing test | Douglas.lv </title>
	<meta name="description" content="Iegādājies Smaržas un kosmētiku,  ādas un matu kopšanas līdzekļus tiešsaistē: Internetveikals: douglas.lv. 
Papildu iespējas: Paraugi, dāvanu iesaiņošana un apsveikuma kartiņa bez maksas, veicot pasūtījumu internetveikalā: www.douglas.lv."/>
	<meta name="keywords" content="Douglas, Douglas Latvia, Internetveikals, Smaržas, Kosmētika, smaržas internetā, kosmētika internetā, Matu kopšana, šampūni, ādas kopšana, krēmi, losjoni."/>
		
		
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link href="https://www.douglas.lv/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/fonts/MyFontsWebfontsKit.css"/>
    <link rel="stylesheet" type="text/css" href="https://www.douglas.lv/fonts/stylesheet.css"/>
	<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/vendors/jqueryUI/jquery-ui-1.8.23.custom.css"/>
	
	<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/vendors/jquery.bxslider/jquery.bxslider.css"/>
	<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/vendors/jScrollPane/style/jquery.jscrollpane.css"/>	
	
		<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/res/css/frontend/content.css?v=2016_02_05"/>
		<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/res/css/default.css?v=2016_02_05"/>
		<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/res/css/style.css?v=2016_02_05"/>
		<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/res/css/frontend/style.css?v=2016_02_05"/>
		<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/res/css/frontend/commerce.css?v=2016_02_05"/>
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js?v=2016_02_05"></script>
		<script type="text/javascript" src="https://www.douglas.lv/res/js/common.js?v=2016_02_05"></script>
		
	<script type="text/javascript" src="https://www.douglas.lv/vendors/jqueryUI/jquery-ui-1.8.23.custom.min.js"></script>
	<script type="text/javascript" src="https://www.douglas.lv/vendors/jquery.textshadow.js"></script>
	<script type="text/javascript" src="https://www.douglas.lv/vendors/fancyBox/jquery.fancybox.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/vendors/fancyBox/jquery.fancybox.css"/>

	<script type="text/javascript" src="https://www.douglas.lv/vendors/jquery.bxslider/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="https://www.douglas.lv/vendors/jScrollPane/script/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="https://www.douglas.lv/vendors/jScrollPane/script/jquery.jscrollpane.min.js"></script>
	

	<link rel="stylesheet" type="text/css" href="https://www.douglas.lv/css/frontend/override.css"/>
	<script type="text/javascript" src="https://www.douglas.lv/res/js/frontend/frontend.js?v=2016_02_05"></script>
	
<script type="text/javascript">
$(function(){
	$('#collections_listing h1 a, #sidebar div.collections_sideblock a').click(function(e){
		var params = {hash:'', cat1:0, cat2:0, cat3:0, brand:0, brand_cat:0};
		$.ajax({
			url: '?set_ref=1', 
			data: params,
			success: function(data){
				//
			},
			async: false
		});
		//e.preventDefault();
	});
	$('div.plist a.prod_el, div.plist a.prod_top').click(function(e){		
		var hash = window.location.hash.replace(/^.*#[_]?/, '');
		var params = {hash:hash, cat1:0, cat2:0, cat3:0, brand:0, brand_cat:0};
		$('#ajax_loader').show();
		$.ajax({
			url: '?set_ref=1', 
			data: params,
			success: function(data){
				//
			},
			async: false
		});
	});
});
</script>

	
	<script>
		
		function hover(element) {
			var str = element.getAttribute("src");
			str = str.substring(0, (str.length)-16)+str.substring((str.length)-4, str.length);
			element.setAttribute('src', str);
		}
		function unhover(element) {
			var str = element.getAttribute("src");
			str = str.substring(0, (str.length)-4) + "_desaturated" + str.substring((str.length)-4, str.length);
			element.setAttribute('src', str);
		}
		
	</script>
	
			
	<!-- Google Tag Manager -->
	
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WDJ96R"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WDJ96R');</script>
	
	<!-- End Google Tag Manager -->
			
			
		<script defer async type="text/javascript" src="//www.draugiem.lv/js/retarget.js" charset="utf-8"></script>
	
		

	
	
	<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '413250762162421']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=413250762162421&amp;ev=PixelInitialized" /></noscript>
			
		
	
	
	
</head>
<body id="type_contacts">
		<div class="side_banner" style="height:340px;">
	<div class="side_banner_title rotate" style="width:340px; text-align:center;">
		Douglas Klienta karte
	</div>
	<span class="side_banner_arrow_l"></span>
	<div class="side_banner_content" style="height:337px;">
		<p><a href="https://www.douglas.lv/lv/douglas-klientu-karte/"><img style="display: block; margin-left: auto; margin-right: auto;" title="Douglas Kliente Karte - Lojalitātes priekšrocības veikalos Douglas" alt="Douglas Kliente Karte - Lojalitātes priekšrocības veikalos Douglas" width="340" height="340" src="https://www.douglas.lv/cache/images/3147a3242eff2743e59d9c5dd50a384f.png"/></a></p>
	</div>
</div>

<script>
	$('.side_banner').on('click',function(e){
		//$(this).css('right',$('.side_banner_content').width());
		//alert($(this).css('right'));	
		if ($(this).css('right') != '0px'){
			$(this).stop().animate({
				right: 0,
			}, 500);
			$('.side_banner_arrow_l').removeClass('side_banner_arrow_r');
		} else {
			$(this).stop().animate({
				right: $('.side_banner_content').width(),
			}, 500);
			$('.side_banner_arrow_l').addClass('side_banner_arrow_r');
		}
	});
</script>


		<div id="page_wrapper" class="lv">
		
				<div class="cookie_bar clearfix">
			<div class="width_wrapper">
				<p>
					Lai mūsu vietnes lietošana Jums būtu nodrošināta pilnvērtīga, mēs izmantojam sīkfailus. Lietojot mūsu vietni, Jūs piekrītat, ka mēs ievietojam sīkfailus jūsu datorā. Uzziniet vairāk: <a href="https://www.douglas.lv/lv/privatumu-politika/">Privātumu politika</a>.									
				</p>
				<div class="close"></div>
			</div>
			
				<script>
					$(document).ready(function(){
						$('.cookie_bar .close').click(function(){
							setCookie('CookiePolicyShown','1','1000');
							
							$(".cookie_bar").fadeOut('fast');
						});
					});
					function setCookie(cname,cvalue,exdays)
					{
						var d = new Date();
						d.setTime(d.getTime()+(exdays*24*60*60*1000));
						var expires = "expires="+d.toGMTString();
						document.cookie = cname + "=" + cvalue + ";domain=douglas.lv;path=/;" + expires;
					}
				</script>
			
		</div>
				
		<div id="head" class="clearfix">
			<div id="head_row1" class="width_wrapper">
				<div class="logo_languages">
				<a id="logo" href="https://www.douglas.lv/">
															<img src="https://www.douglas.lv/images/frontend/logo_douglas_lt.png" alt="Douglas" />
				</a>
								<div id='languages_menu'>
					<ul>
						<li>
							<a class="active" href="https://www.douglas.lv/">Latviešu<img src="https://www.douglas.lv/images/frontend/lv.png"/></a>
						</li>
						<li>
							<a class="" href="https://www.douglas.lv/ru">Русский<img src="https://www.douglas.lv/images/frontend/ru.png"/></a>
						</li>
					</ul>
				</div>
				</div>
								<div id="cart_block">
		<a href="https://www.douglas.lv/lv/mans-konts/grozs/" rel="nofollow">
		<span class="cart_btn">GROZS</span>
				<span class="amount">0</span>
		
		<span class="total">&euro; 0</span>
	</a>
	<div class="overview_container empty">
		<div class="overview clearfix">
			<em class="arrow"></em>
									
			<div class="clearfix" style="padding-bottom: 10px; margin-top: 10px;margin-left:10px;margin-right:10px; border-bottom:1px solid lightgrey;">
				<div style="float:left;  height:28px; line-height: 28px; font-weight: bold;">Kopējā summa:</div>
				<div style="float:right; height:28px; line-height: 28px; font-weight: bold;">0 &euro;</div>
			</div>
			
			<div class="clearfix">
												<table style="width:100%; padding: 12px;">
					<tr>
						<td style="vertical-align:middle; text-align: left; width: 45px;">
																<img src="https://www.douglas.lv/images/frontend/commerce/free_delivery_now.png">
														</td>
						<td style="vertical-align: bottom; text-align: left;">
							<span class="text">
																Līdz <span style="color:#2ea480; font-weight:bold;">BEZMAKSAS</span> piegādei:<span style='color: #e5378f; font-weight: bold;'> 39.95 &euro;</span>
															</span>
						</td>
					</tr>
				</table>
			</div>
										
			<div class="clearfix">
						<a class="buy" href='https://www.douglas.lv/lv/mans-konts/grozs/'><img src="https://www.douglas.lv/images/frontend/basket.png" style="position: absolute; top: -5px; left: -35px;">PIRKT</a>
			</div>
					</div>
	</div>
</div>
				<div id="login_area" >
	<div class="wrp1">
		<b>Sveicināti,</b><br/>
		<a class="login_link" href="https://www.douglas.lv/lv/mans-konts/pasutijumi/?page=pieslegties" rel="nofollow" >ienākt</a>
		vai
		<a class="login_link" href="https://www.douglas.lv/lv/mans-konts/pasutijumi/?page=registreties" rel="nofollow" >reģistrēties</a>
	</div>
</div>
				
				<div id="service_wrapper">					
					<div id="service">
							<a href="https://www.douglas.lv/lv/klientu-karte/">Klientu karte</a>
	<span class="sep">|</span>	<a href="https://www.douglas.lv/lv/davanu-kartes-bilance/">Dāvanu kartes bilance</a>
	<span class="sep">|</span>	<a href="https://www.douglas.lv/lv/make-up-skola/">Make-up skola</a>
	<span class="sep">|</span>	<a href="https://www.douglas.lv/lv/kontakti/veikali/">Veikali</a>
	
					</div>
					<div id="search">
						<form action="https://www.douglas.lv/lv/meklesana/" method="get">
														<input id="searchinput" autocomplete="off" class="text clear_on_focus" type="text" name="s" value="Meklēt"/>
														<input class="submit" type="submit" value=" "/>
						</form>
						<div id="autocomplete"></div>
						
						<script type="text/javascript">
						$(function(){

							$("#searchinput").keyup(function(){
								var val = $(this).val();
								if (val.length > 2) {
									$.ajax({
										type: 'POST',
										url: '?display=content_types/pages/autocomplete.tpl',
										data: {keyword: val},
										success: function(data) {											
											$("#autocomplete").html(data).show();
										}
									});
								}
							});
							
							/*
							var keywords = [];
							$.getJSON('https://www.douglas.lv/files/search_ac.txt', function(data) {				           
						        $.each(data, function(i, obj) {                
						            keywords.push(obj);                
						        });   

						        keywords.sort(function(a, b) {
						            return a.label > b.label;
						        });							        												
								
						        $( "#searchinput" ).autocomplete({
					    			minLength: 2,
					    			source: function(request, response) {								        	
							            var results = $.ui.autocomplete.filter(keywords, request.term);
							            response(results.slice(0, 20));
							        },
					    			focus: function( event, ui ) {
					    				$( "#searchinput" ).val( ui.item.label );
					    				return false;
					    			},
					    			select: function( event, ui ) {						    				
										var url = 'https://www.douglas.lv/';
										if ( ui.item.type == 'product' ) {
											url += 'katalogas/'+ui.item.value;
										} else {
											url += ui.item.value;
										}
										window.location = url;
					    				return false;
					    			},
					    			open: function(e,ui) {
							        	$(this).autocomplete('widget').css('z-index', 200);				                
							            var acData = $(this).data('autocomplete');
							            var termTemplate = "<span class='bold'>%s</span>";
							            acData.menu.element.find('a')
							            .each(function() {
							                var me = $(this);
							                var regex = new RegExp(acData.term, "gi");
							                me.html( me.text().replace(regex, function (matched) {
							                    return termTemplate.replace('%s', matched);
							                }) );
							            });
							        }
					    		})
					    		.data( "autocomplete" )._renderItem = function( ul, item ) {
						        	brand = '';
						    		if (item.type == 'brand') {
										brand = 'Prekės ženklas: ';
						    		}
					    			return $( "<li></li>" )
					    				.data( "item.autocomplete", item )
					    				.append( "<a>" + brand + item.label + "</a>" )
					    				.appendTo( ul );
					    		};
						        
						    });	
							*/ 
						});
						</script>
						
					</div>
				</div>
							</div>
			<div id="head_row2">
				<div class="width_wrapper">
					<div id="catalog_menu">
	<ul class="level1">		
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/smarzas/" class="level1" ><span >Smaržas</span></a>
									<div class="submenu">
						<ul>
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/sieviesu/">Sieviešu </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/viriesu/">Vīriešu </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/parfimeti-dezodoranti/">Parfimēti dezodoranti </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/parfimetas-dusas-zelejas/">Parfimētas dušas želejas </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/bernu/">Bērnu </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/parfimeti-kermena-produkti/">Parfimēti ķermeņa produkti </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/smarzas/aksesuars-reload-aromatu-flakons/">Aksesuārs: reload.™ - aromātu flakons </a></li>						
							
						</ul>					
					</div>
							</li>
			
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/" class="level1" ><span >Ādas kopšana</span></a>
									<div class="submenu">
						<ul>
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/seja/">Seja </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/kermenis/">Ķermenis </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/rokas-kajas/">Rokas/kājas </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/pirms-saulosanas-pec-saulosanas/">Pirms sauļošanās/pēc sauļošanās </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/skaistumkopsanas-ierices/">Skaistumkopšanas ierīces </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/bernu/">Bērnu </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/adas-kopsana/viriesu/">Vīriešu </a></li>						
							
						</ul>					
					</div>
							</li>
			
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/make-up/" class="level1" ><span >Make-up</span></a>
									<div class="submenu">
						<ul>
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/make-up/seja/">Seja </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/make-up/acis/">Acis </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/make-up/nagi/">Nagi </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/make-up/lupas/">Lūpas </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/make-up/aksesuari/">Aksesuāri </a></li>						
							
						</ul>					
					</div>
							</li>
			
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/" class="level1" ><span >Matu kopšana</span></a>
									<div class="submenu">
						<ul>
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/mazgasana/">Mazgāšana </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/veidosana/">Veidošana </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/viriesu/">Vīriešu </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/bernu/">Bērnu </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/matu-kopsanas-ierices/">Matu kopšanas ierīces </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/matu-kopsana/matu-krasas/">Matu krāsas </a></li>						
							
						</ul>					
					</div>
							</li>
			
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/aksesuari/" class="level1 xmas" ><span >Aksesuāri</span></a>
									<div class="submenu">
						<ul>
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/zobu-kopsana/">Zobu kopšana </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/vanna-dusa/">Vanna/duša </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/skusanas/">Skūšanās </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/grims/">Grims </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/maskas-gulesanai/">Maskas gulēšanai </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/matu-aksesuari/">Matu aksesuāri </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/manikirs/">Manikīrs </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/rotaslietas/">Rotaslietas </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/komplekti/">Komplekti </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/saulesbrilles/">Saulesbrilles </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/aksesuari-majdzivniekiem/">Aksesuāri mājdzīvniekiem </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/aksesuari-majai/">Aksesuāri mājai </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/davanu-karte/">Dāvanu karte </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/aksesuari/cits/">Cits </a></li>						
							
						</ul>					
					</div>
							</li>
			
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/" class="level1" ><span >Dāvanu pasaule</span></a>
									<div class="submenu">
						<ul>
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/davanas-vinai/">Dāvanas VIŅAI </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/davanas-vinam/">Dāvanas VIŅAM </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/davana-berniem/">Dāvana bērniem </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/skaistumkopsanas-ierices/">Skaistumkopšanas ierīces </a></li>						
													<li class="level2"><a href="https://www.douglas.lv/lv/katalogs/davanu-pasaule/ekskluzivas-davanas/">Ekskluzīvas dāvanas </a></li>						
							
						</ul>					
					</div>
							</li>
			
					<li class="level1">
				<a href="https://www.douglas.lv/lv/katalogs/b2b/" class="level1" ><span >B2B</span></a>
							</li>
			
				<li class="level1 ">
			<a class="level1 blog_link" href="http://blog.douglas.lv/" target="_blank"><span>Blogs</span></a>
		</li>
		<li class="level1 brands_menu">
	<a class="level1" href="https://www.douglas.lv/lv/zimoli/"><span>ZĪMOLI</span></a>
	<div class="submenu brands">
		<div class="abc">
            <table style="width:100%">
                <tr>
                                            <td>
                            <a id="letter_A" class="brand_list_top_letter" href="#A">
                                A
                            </a>
                        </td>
                                            <td>
                            <a id="letter_B" class="brand_list_top_letter" href="#B">
                                B
                            </a>
                        </td>
                                            <td>
                            <a id="letter_C" class="brand_list_top_letter" href="#C">
                                C
                            </a>
                        </td>
                                            <td>
                            <a id="letter_D" class="brand_list_top_letter" href="#D">
                                D
                            </a>
                        </td>
                                            <td>
                            <a id="letter_E" class="brand_list_top_letter" href="#E">
                                E
                            </a>
                        </td>
                                            <td>
                            <a id="letter_F" class="brand_list_top_letter" href="#F">
                                F
                            </a>
                        </td>
                                            <td>
                            <a id="letter_G" class="brand_list_top_letter" href="#G">
                                G
                            </a>
                        </td>
                                            <td>
                            <a id="letter_H" class="brand_list_top_letter" href="#H">
                                H
                            </a>
                        </td>
                                            <td>
                            <a id="letter_I" class="brand_list_top_letter" href="#I">
                                I
                            </a>
                        </td>
                                            <td>
                            <a id="letter_Y" class="brand_list_top_letter" href="#Y">
                                Y
                            </a>
                        </td>
                                            <td>
                            <a id="letter_J" class="brand_list_top_letter" href="#J">
                                J
                            </a>
                        </td>
                                            <td>
                            <a id="letter_K" class="brand_list_top_letter" href="#K">
                                K
                            </a>
                        </td>
                                            <td>
                            <a id="letter_L" class="brand_list_top_letter" href="#L">
                                L
                            </a>
                        </td>
                                            <td>
                            <a id="letter_M" class="brand_list_top_letter" href="#M">
                                M
                            </a>
                        </td>
                                            <td>
                            <a id="letter_N" class="brand_list_top_letter" href="#N">
                                N
                            </a>
                        </td>
                                            <td>
                            <a id="letter_O" class="brand_list_top_letter" href="#O">
                                O
                            </a>
                        </td>
                                            <td>
                            <a id="letter_P" class="brand_list_top_letter" href="#P">
                                P
                            </a>
                        </td>
                                            <td>
                            <a id="letter_R" class="brand_list_top_letter" href="#R">
                                R
                            </a>
                        </td>
                                            <td>
                            <a id="letter_S" class="brand_list_top_letter" href="#S">
                                S
                            </a>
                        </td>
                                            <td>
                            <a id="letter_T" class="brand_list_top_letter" href="#T">
                                T
                            </a>
                        </td>
                                            <td>
                            <a id="letter_U" class="brand_list_top_letter" href="#U">
                                U
                            </a>
                        </td>
                                            <td>
                            <a id="letter_V" class="brand_list_top_letter" href="#V">
                                V
                            </a>
                        </td>
                                            <td>
                            <a id="letter_W" class="brand_list_top_letter" href="#W">
                                W
                            </a>
                        </td>
                                            <td>
                            <a id="letter_Z" class="brand_list_top_letter" href="#Z">
                                Z
                            </a>
                        </td>
                                    </tr>
            </table>
		</div>
        <div id="brand_slider_left_arrow"><a></a></div>
        <div id="brand_slider_right_arrow"><a></a></div>
        <div class="brands_list">
                                	
                <div class="bcol">                                                                <div id="A" class="letter">A</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/abercrombie-fitch/">
                                Abercrombie &amp; Fitch
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/accentra/">
                                Accentra
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/agent-provocateur/">
                                Agent Provocateur
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/ala-a-paris/">
                                Alaïa Paris
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/alterna/">
                                Alterna
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/angry-bird/">
                                Angry Bird
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/antonio-banderas/">
                                Antonio Banderas
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/argan-5/">
                                Argan 5
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/armand-basi/">
                                Armand Basi
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/artdeco/">
                                Artdeco
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/artemis/">
                                Artemis
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/attirance/">
                                Attirance
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/avengers/">
                                Avengers
                            </a>
                        
                                    
            	
                                                                                <div id="B" class="letter">B</div>
                                                                                </div>
                                            
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/baylis-harding/">
                                Baylis &amp; Harding
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/baldessarini/">
                                Baldessarini
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/balmain/">
                                Balmain
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/banana-republic/">
                                Banana Republic
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/baratti/">
                                Baratti
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/barbara-hofmann/">
                                Barbara Hofmann
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/barbie/">
                                Barbie
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/beauty-blender/">
                                Beauty Blender
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/beyonc/">
                                Beyoncé
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/bentley/">
                                Bentley
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/bio2you/">
                                Bio2You
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/biosilk/">
                                BioSilk
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/biotherm/">
                                Biotherm
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/blend-oud/">
                                Blend Oud
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/bobbi-brown/">
                                Bobbi Brown
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/bodycare-deli/">
                                Bodycare Deli
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/boucheron/">
                                Boucheron
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/bourjois/">
                                Bourjois
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/burberry/">
                                Burberry
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/bvlgari/">
                                BVLGARI
                            </a>
                        
                                    
            	
                                                                                <div id="C" class="letter">C</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/chabaud-maison-de-parfum/">
                                Chabaud Maison de Parfum
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/california-scents/">
                                California Scents
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/calvin-klein/">
                                Calvin Klein
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/carolina-herrera/">
                                Carolina Herrera
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/carven/">
                                Carven
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/chaugan/">
                                Chaugan
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/ceano-cosmetics/">
                                CEANO cosmetics
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/chi/">
                                CHI
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/clarins/">
                                Clarins
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/clarisonic/">
                                Clarisonic
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/clinique/">
                                Clinique
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/chlo/">
                                Chloé
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/coach/">
                                Coach
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/collistar/">
                                Collistar
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/color-par-rodolphe/">
                                Coloré Par Rodolphe
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/creightons/">
                                Creightons
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/christian-breton/">
                                Christian Breton
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/christian-faye/">
                                Christian Faye
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/cristiano-ronaldo/">
                                Cristiano Ronaldo
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/christina-aguilera/">
                                Christina Aguilera
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/chupa-chups/">
                                Chupa Chups
                            </a>
                        
                                    
            	
                                                                                <div id="D" class="letter">D</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/d-g/">
                                D&amp;G
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/daytox/">
                                Daytox
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/davidoff/">
                                Davidoff
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/desigual/">
                                Desigual
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/diesel/">
                                Diesel
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/dior/">
                                Dior
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/disney-cars/">
                                Disney Cars
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/disney-princess/">
                                Disney Princess
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/dkny/">
                                DKNY
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/dolce-gabbana/">
                                Dolce&amp;Gabbana
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/douglas/">
                                Douglas
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-aquafocus/">
                                Douglas AquaFocus
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-beauty-system/">
                                Douglas Beauty System
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-fragrances/">
                                Douglas Fragrances
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-hair/">
                                Douglas Hair
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-home-spa/">
                                Douglas Home SPA
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-les-d-lices/">
                                Douglas Les Délices
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-make-up/">
                                Douglas Make - Up
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-men/">
                                Douglas Men
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-nails-hands-feet/">
                                Douglas nails, hands, feet
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-naturals/">
                                Douglas Naturals
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-sun/">
                                Douglas Sun
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/douglas-xl-xs/">
                                Douglas XL/xs
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/dsquared/">
                                Dsquared²
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/dunhill/">
                                Dunhill
                            </a>
                        
                                    
            	
                                                                                <div id="E" class="letter">E</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/eisenberg/">
                                EISENBERG
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/elie-saab/">
                                Elie Saab
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/emanuel-ungaro/">
                                Emanuel Ungaro
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/eos/">
                                EOS
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/escada/">
                                Escada
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/escentric-molecules/">
                                Escentric Molecules
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/est-e-lauder/">
                                Estée Lauder
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/evody-parfums-paris/">
                                Evody Parfums Paris
                            </a>
                        
                                    
            	
                                                                                <div id="F" class="letter">F</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/fenjal/">
                                Fenjal
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/floid/">
                                Floid
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/foreo/">
                                Foreo
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/forster-johnsen/">
                                Förster &amp; Johnsen
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/frenesies/">
                                Frenesies
                            </a>
                        
                                    
            	
                                                                                <div id="G" class="letter">G</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/gabriela-sabatini/">
                                Gabriela Sabatini
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/giorgio-armani/">
                                Giorgio Armani
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/giovanni/">
                                Giovanni
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/girlz-only/">
                                Girlz Only
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/givenchy/">
                                Givenchy
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/glamglow/">
                                GlamGlow
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/grace-cole/">
                                Grace Cole
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/gucci/">
                                Gucci
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/guess/">
                                Guess
                            </a>
                        
                                    
            	
                                                                                <div id="H" class="letter">H</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/hayari/">
                                HAYARI
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/halloween/">
                                HALLOWEEN
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/happy-hair-days/">
                                Happy Hair Days
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/hask/">
                                Hask
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/heathcote-ivory/">
                                Heathcote&amp;Ivory
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/hello-kitty/">
                                Hello Kitty
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/hercules-sagemann/">
                                Hercules Sägemann
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/herm-s/">
                                Hermès
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/hugh-parsons/">
                                Hugh Parsons
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/hugo-boss/">
                                Hugo Boss
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/hurraw/">
                                Hurraw
                            </a>
                        
                                    
            	
                                                                                <div id="I" class="letter">I</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/i-love/">
                                I Love
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/india-i-c-o-n-linija/">
                                India I.C.O.N. līnija
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/invisibobble/">
                                Invisibobble
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/isadora/">
                                IsaDora
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/issey-miyake/">
                                Issey Miyake
                            </a>
                        
                                    
            	
                                                                                <div id="Y" class="letter">Y</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/yves-saint-laurent/">
                                Yves Saint Laurent
                            </a>
                        
                                    
            	
                                                                                <div id="J" class="letter">J</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/j-s-douglas-sohne/">
                                J.S. Douglas Söhne
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/james-bond/">
                                James Bond
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/jean-paul-gaultier/">
                                Jean Paul Gaultier
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/jimmy-choo/">
                                Jimmy Choo
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/john-frieda/">
                                John Frieda
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/john-richmond/">
                                John Richmond
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/john-varvatos/">
                                John Varvatos
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/joico/">
                                Joico
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/juicy-couture/">
                                Juicy Couture
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/justin-bieber/">
                                Justin Bieber
                            </a>
                        
                                    
            	
                                                                                <div id="K" class="letter">K</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/kardashian-beauty/">
                                Kardashian Beauty
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/karl-lagerfeld/">
                                Karl Lagerfeld
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/katy-perry/">
                                Katy Perry
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/keep-crazy-and-love-on/">
                                Keep Crazy And Love On!
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/kids-stuff-crazy/">
                                Kids Stuff Crazy
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/kinetics/">
                                Kinetics
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/knots-no-more/">
                                Knots No More
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/kokeshi/">
                                Kokeshi
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/konjac-sponge/">
                                Konjac Sponge
                            </a>
                        
                                    
            	
                                                                                <div id="L" class="letter">L</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/l-action-paris/">
                                L&#039;action Paris
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lacoste/">
                                Lacoste
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lady-gaga/">
                                Lady Gaga
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lalique/">
                                Lalique
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lanc-me/">
                                Lancôme
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lanvin/">
                                Lanvin
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lip-smacker/">
                                Lip Smacker
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lorenay/">
                                Lorenay
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/lumene/">
                                Lumene
                            </a>
                        
                                    
            	
                                                                                <div id="M" class="letter">M</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/mac/">
                                MAC
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/macho-beard-company/">
                                Macho Beard Company
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/madara/">
                                Madara
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/mades-cosmetics/">
                                Mades Cosmetics
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/makeup-eraser/">
                                MakeUp Eraser
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/marc-jacobs/">
                                Marc Jacobs
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/marina-de-bourbon/">
                                Marina De Bourbon
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/marmara-sterling/">
                                Marmara Sterling
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/marvis/">
                                Marvis
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/matis/">
                                Matis
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/max-factor/">
                                Max Factor
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/mercedes-benz/">
                                Mercedes-Benz
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/mexx/">
                                Mexx
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/mia-and-me/">
                                Mia and Me
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/michael-kors/">
                                Michael Kors
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/mitomo/">
                                MITOMO
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/miu-miu/">
                                Miu Miu
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/molinard/">
                                Molinard
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/montblanc/">
                                MontBlanc
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/moschino/">
                                Moschino
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/muhle/">
                                Mühle
                            </a>
                                                                                </div>
                        
                                    
            	
                <div class="bcol">                                                                <div id="N" class="letter">N</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/naked/">
                                Naked
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/naomi-campbell/">
                                Naomi Campbell
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/narciso-rodriguez/">
                                Narciso Rodriguez
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nejma-collection/">
                                Nejma Collection
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nesti-dante/">
                                Nesti Dante
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nicky-clarke/">
                                Nicky Clarke
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nina-ricci/">
                                Nina Ricci
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nyx/">
                                NYX
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/noah/">
                                NOAH
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/noops/">
                                NoOPS
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nouba/">
                                Nouba
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/nspa/">
                                NSPA
                            </a>
                        
                                    
            	
                                                                                <div id="O" class="letter">O</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/officina-delle-essenze/">
                                Officina delle Essenze
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/origins/">
                                Origins
                            </a>
                        
                                    
            	
                                                                                <div id="P" class="letter">P</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/paco-rabanne/">
                                Paco Rabanne
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/payot/">
                                Payot
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/paolo-cane/">
                                Paolo Cane
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/phil-smith/">
                                Phil Smith
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/philips/">
                                Philips
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/pien-dairy-spa/">
                                Pien-DAIRY SPA
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/prada/">
                                Prada
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/proraso/">
                                Proraso
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/puma/">
                                Puma
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/pussy-deluxe/">
                                Pussy Deluxe
                            </a>
                        
                                    
            	
                                                                                <div id="R" class="letter">R</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/real-techniques/">
                                REAL TECHNIQUES
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/reload/">
                                Reload
                            </a>
                                                                                </div>
                        
                    
                        <div class="bcol">                                                        <a class="brand_item" href="https://www.douglas.lv/lv/rich/">
                                RICH
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/roberto-cavalli/">
                                Roberto Cavalli
                            </a>
                        
                                    
            	
                                                                                <div id="S" class="letter">S</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/s-t-dupont/">
                                S.T Dupont
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/salvador-dali/">
                                Salvador Dali
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/salvatore-ferragamo/">
                                Salvatore Ferragamo
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/sensai/">
                                Sensai
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/serge-lutens/">
                                Serge Lutens
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/shakira/">
                                Shakira
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/shiseido/">
                                Shiseido
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/sisley/">
                                Sisley
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/solingen/">
                                Solingen
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/spatopia/">
                                SPAtopia
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/starck-paris/">
                                STARCK PARIS
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/stenders/">
                                STENDERS
                            </a>
                        
                                    
            	
                                                                                						</div>
						<div class="bcol">
                        <div id="T" class="letter">T</div>
                    
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tabac/">
                                Tabac
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tangle-angel/">
                                Tangle Angel
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tangle-teezer/">
                                Tangle Teezer
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/teaology/">
                                TEAology
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/ted-lapidus/">
                                Ted Lapidus
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/teo-cabanel/">
                                Teo Cabanel
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/thalgo/">
                                Thalgo
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/thierry-mugler/">
                                Thierry Mugler
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tigi/">
                                Tigi
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tom-ford/">
                                Tom Ford
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tommy-hilfiger/">
                                Tommy Hilfiger
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/toni-gard/">
                                Toni Gard
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tous/">
                                Tous
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/trevor-sorbie/">
                                Trevor Sorbie
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/tropical/">
                                Tropical
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/trussardi/">
                                Trussardi
                            </a>
                                                                                </div>
                        
                                    
            	
                <div class="bcol">                                                                <div id="U" class="letter">U</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/ultimate-spiderman/">
                                Ultimate Spiderman
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/urban-decay/">
                                Urban Decay
                            </a>
                        
                                    
            	
                                                                                <div id="V" class="letter">V</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/valentino/">
                                Valentino
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/van-cleef-arpels/">
                                Van Cleef &amp; Arpels
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/venus/">
                                Venus
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/versace/">
                                Versace
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/viktor-rolf/">
                                Viktor&amp;Rolf
                            </a>
                        
                                    
            	
                                                                                <div id="W" class="letter">W</div>
                                            
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/wonderstripes/">
                                Wonderstripes
                            </a>
                        
                                    
            	
                                                                                						</div>
						<div class="bcol">
                        <div id="Z" class="letter">Z</div>
                    
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/zadig-voltaire/">
                                Zadig &amp; Voltaire
                            </a>
                        
                    
                                                                                <a class="brand_item" href="https://www.douglas.lv/lv/zoo-paris/">
                                Zooé Paris
                            </a>
                        
                                    
                                    
                    </div>
	</div>
</li>

    <script type="text/javascript">
        $( document ).ready(function() {
           /*$('.submenu.brands').mouseleave(function() {
                setTimeout(function () {
                    $('#catalog_menu li.brands_menu .submenu').hide();
                }, 3000);
           });*/
           $('.submenu.brands td a#letter_A').addClass('active');
           $(".submenu.brands td a.brand_list_top_letter").click(function(e) {
                e.preventDefault();
                $('.submenu.brands td a.brand_list_top_letter').removeClass('active');
                $(this).addClass('active');
                var fullelementId = $(this).attr('id');
                var elementId = fullelementId.replace("letter_", "");
                var letterposition = $('#' + elementId).position().left;
                var full_left = 0 - letterposition + 10;  
                $('.submenu.brands .brands_list').animate({ left: full_left }, function() {
            });
           });
        });
        function product_slider(){
            $('.submenu.brands .brands_list').css('left','0');
            var container_width = $('#catalog_menu .submenu.brands').width();
            var brands_columns_count = 5;
            
            $("#brand_slider_right_arrow a").on('click', function() {
            var total_brand_columns = $('.brands_list .bcol').length;
            var brand_column_width = $('.brands_list .bcol').outerWidth(true);
            var wrpleft = $('.submenu.brands .brands_list').position().left;
            var full_left = wrpleft - brand_column_width - 25;
            var slide_count = Math.abs(full_left / brand_column_width)+brands_columns_count;
            if(slide_count > total_brand_columns){
                full_left = 0;
            }
            $('.submenu.brands .brands_list').animate({ left: full_left }, function() {
            });
        });
         $("#brand_slider_left_arrow a").on('click', function() {
            var total_brand_columns = $('.brands_list .bcol').length;
            var brand_column_width = $('.brands_list .bcol').outerWidth(true);
            var wrpleft = $('.submenu.brands .brands_list').position().left;
            var full_left = wrpleft + brand_column_width -25;   
            var slide_count = full_left / brand_column_width;
            if(full_left > 0){
                full_left = ((total_brand_columns-brands_columns_count) * brand_column_width)* (-1);
            }
            $('.submenu.brands .brands_list').animate({ left: full_left }, function() {
            });
        });
        
        }
        $(function(){
        product_slider();
    });
    </script>

	</ul>
	</div>

<script type="text/javascript">
	$(function(){
		
	});
</script>

				</div>
			</div>
				
			<div id="head_row3">
				
				<div class="width_wrapper clearfix" style="margin:10px auto;">
					
					<div class="item">
						<div><span>Bezmaksas apsveikumu kartiņa</span></div>
					</div>
					<div class="item">
						<div><span>Bezmaksas dāvanu saiņošana</span></div>
					</div>
					<div class="item">
						<div><span>2 bezmaksas paraugi</span></div>
					</div>
					<div class="item last">
						<div>
						<span>
																									<b style="font-size:12px;">Bezmaksas piegāde no 39,95 EUR.</b>
												</span>
						</div>
					</div>
					<div class="long_item">
						Piegāde visā Latvijā 1-2 darba dienas!
Jautājumi: sūtiet uz: <a href="mailto:eveikals@douglaslatvia.lv">eveikals@douglaslatvia.lv</a> vai zvaniet pa tālr. +371 67199299. Te ir darbs: <a href="http://www.douglas.lv/lv/par-douglas/karjera/" target="_blank">Karjera</a>
					</div>
						
									</div>

			</div>
					</div>

		<div id="content_layout">
			<div class="width_wrapper clearfix">
					<div id="content_layout_outter" class=" clearfix">
<div id="page_path">				<a href="https://www.douglas.lv/lv/">Douglas</a>
		<span class="separator">/</span>			<a href="https://www.douglas.lv/lv/landing-test/">landing test</a>
	</div>
<div id="sidebar" class="">
					<div class="sideblock">
	<h2>Informācija</h2>
		<div class="level1 has_childs		 ">
		<a class="level1" href="https://www.douglas.lv/lv/par-douglas/">
			<span>Par Douglas</span>
					</a>
		
			</div>
		<div class="level1 has_childs		 ">
		<a class="level1" href="https://www.douglas.lv/lv/noteikumi-2/">
			<span>Noteikumi</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/apmaksa-rekviziti/">
			<span>Apmaksa - Rekvizīti</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/piegade/">
			<span>Piegāde</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/klientu-karte/">
			<span>Klientu karte</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/davanu-kartes-bilance/">
			<span>Dāvanu kartes bilance</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/make-up-skola/">
			<span>Make-up skola</span>
					</a>
		
			</div>
		<div class="level1 has_childs		 ">
		<a class="level1" href="https://www.douglas.lv/lv/jaunumi-un-pasakumi/">
			<span>Jaunumi un pasākumi</span>
					</a>
		
			</div>
		<div class="level1 has_childs		 ">
		<a class="level1" href="https://www.douglas.lv/lv/douglas-katalogs/">
			<span>Douglas Katalogs</span>
					</a>
		
			</div>
		<div class="level1 has_childs		 ">
		<a class="level1" href="https://www.douglas.lv/lv/kontakti/">
			<span>Kontakti</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/menesa-piedavajumi/">
			<span>Mēneša piedāvājumi</span>
					</a>
		
			</div>
		<div class="level1		 ">
		<a class="level1" href="https://www.douglas.lv/lv/privatumu-politika/">
			<span>Privātumu Politika</span>
					</a>
		
			</div>
	</div>

								</div>
	

<div id="main_content"  >
    		<h1 class="page_title">landing test
			</h1>
		
	<div id="content_type_content">
					<p style="margin: 0px; background-color: #b6dccb;"><iframe height="1192" src="https://www.douglas.lv/vendors/2016/landing.php" style="border: 0;" width="960"></iframe></p>
<script type="text/javascript">// <![CDATA[
$('#page_path').hide();
$('#sidebar').hide();
$('#main_content').css('padding','0').css('width','960px');
$('.page_title').hide();
// ]]></script>
<div id="snowflakeContainer"><img class="snowflake" src="https://www.douglas.lv/vendors/2016/images/snowflake.png" alt="" /></div>


						</div>
</div>
</div>

<script>
$('body').on('hover', '#cart_block', function(e){
	var element = $('#cart_block .overview_container:not(.empty)');
		if(element.css('z-index') === '1000')
			$('#cart_block .overview_container:not(.empty)').fadeIn('fast');
		else
			element.css('z-index','1000');
	});

	$('body').on('mouseleave', '#cart_block', function(e){
		$('#cart_block .overview_container').fadeOut('fast');
	});
	
	$('body').on('click', '#overview_close', function(e){
	$('.overview_container').css('z-index','1001').fadeOut('fast');
	});
</script>

	
			</div>
		</div>

		<div id="footer">			
			<div class="width_wrapper clearfix">
				<div class="both clearfix">
					<div class="right clearfix">
						<div id="newsletter">
	<h2 class="title">Douglas jaunumi</h2>
	<div class="inner">
		<div class="desc">
			Uzzini pirmais par Douglas jaunumiem, bonusiem un īpašajiem piedāvājumiem
		</div>
		<form action="#" method="post" class="clearfix">
			<input type="hidden" name="state" value="newsletter" />
			<input type="hidden" name="source" value="" />
			<input type="hidden" name="group" value="footer_block" />
			<input class="text clear_on_focus" type="text" name="email" value="E-pasts"/>
			<input class="submit" type="submit" value=""/>
		</form>
	</div>
</div>

<script type="text/javascript">
$('#newsletter form').submit(function(e){
	e.preventDefault();
	var url = '?return=json&display=content_types/pages/newsletter.tpl';
	$('#ajax_loader').show();
	$.post(url, $(this).serialize(), function(data){
		$('#ajax_loader').hide();
		// console.log(data);
		if (data['ok_msg'] != undefined) {
			$('#newsletter .desc').removeClass('error').text(data['ok_msg']);
			alert(data['ok_msg']);
		} else
		if (data['error_msg'] != undefined) {
			alert(data['error_msg']);
		}
		
	}, 'json');
});
</script>

										
						<div class="clear"></div>					
												<div id="info_social" class="footer_block">
														<h2 class="title">seko mums</h2>
							<ul class="menu clearfix"><li class="alias_youtube "><a target="_blank" href="http://www.youtube.com/user/DouglasLatvia" ><span>Youtube</span></a></li><li class="alias_facebook "><a target="_blank" href="https://www.facebook.com/pages/Douglas-Latvia/243128575825324" ><span>Facebook</span></a></li><li class="alias_draugiem "><a target="_self" href="http://www.draugiem.lv/douglas/" ><span>Draugiem</span></a></li><li class="alias_instagram  last"><a target="_blank" href="https://instagram.com/douglas_latvia/" ><span>Instagram</span></a></li></ul>
						</div>				
												
						<div class="footer_block payment_types_wrapper">
							<div>
							<h2 class="title">Maksājuma metodes</h2>
							<a href="https://www.douglas.lv/lv/apmaksa-rekviziti/">
								<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/Swedbank__text_refl_mynt_desaturated.png">
							</a>
							<a href="https://www.douglas.lv/lv/apmaksa-rekviziti/">
							<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/seb_logo_oben-1_desaturated.jpg">
							</a>
							<a href="https://www.douglas.lv/lv/apmaksa-rekviziti/">
							<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/nordea_desaturated.png">
							</a>
							<a href="https://www.douglas.lv/lv/apmaksa-rekviziti/">
							<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/Visa_Logo_desaturated.png">
							</a>
							<a href="https://www.douglas.lv/lv/apmaksa-rekviziti/">
							<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/4224_3461_MasterCard_desaturated.png">
							</a>
							</div>
							<div>
								<h2 class="title">Drošības garantija</h2>
								<div>
									<a href="http://www.visaeurope.com/">
										<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/Verified_by_VISA_desaturated.png">
									</a>
									<a href="http://www.mastercard.com/lv/consumer/index.html">
										<img onmouseover="hover(this);" onmouseout="unhover(this);" src="https://www.douglas.lv/images/frontend/pyment_types/MasterCard_SecureCode_desaturated.png">
									</a>
								</div>
							</div>
						</div>			
						
					</div>
					<div class="left clearfix">				
						<div id="info_menu" class="footer_block">
							<h2 class="title">INFORMĀCIJA</h2>
							<ul class="menu clearfix"><li class="alias_ "><a target="_self" href="https://www.douglas.lv/lv/par-douglas/" ><span>Par Douglas</span></a></li><li class="alias_rules "><a target="_self" href="https://www.douglas.lv/lv/noteikumi-2/" ><span>Noteikumi</span></a></li><li class="alias_payment "><a target="_self" href="https://www.douglas.lv/lv/apmaksa-rekviziti/" ><span>Apmaksa - Rekvizīti</span></a></li><li class="alias_delivery "><a target="_self" href="https://www.douglas.lv/lv/piegade/" ><span>Piegāde</span></a></li><li class="alias_giftcard "><a target="_self" href="https://www.douglas.lv/lv/klientu-karte/" ><span>Klientu karte</span></a></li><li class="alias_loyalty "><a target="_self" href="https://www.douglas.lv/lv/davanu-kartes-bilance/" ><span>Dāvanu kartes bilance</span></a></li><li class="alias_toni_gard_loterija "><a target="_self" href="https://www.douglas.lv/lv/make-up-skola/" ><span>Make-up skola</span></a></li><li class="alias_ "><a target="_self" href="https://www.douglas.lv/lv/jaunumi-un-pasakumi/" ><span>Jaunumi un pasākumi</span></a></li><li class="alias_ "><a target="_self" href="https://www.douglas.lv/lv/douglas-katalogs/" ><span>Douglas Katalogs</span></a></li><li class="alias_contacts "><a target="_self" href="https://www.douglas.lv/lv/kontakti/" ><span>Kontakti</span></a></li><li class="alias_ "><a target="_self" href="https://www.douglas.lv/lv/menesa-piedavajumi/" ><span>Mēneša piedāvājumi</span></a></li><li class="alias_  last"><a target="_self" href="https://www.douglas.lv/lv/privatumu-politika/" ><span>Privātumu Politika</span></a></li></ul>
						</div>
						<div id="info_stamp">
							<div class="inner">
								<h2 class="title">IEPĒRKOTIES INTERNETĀ</h2>
<ul>
<li>2 bezmaksas paraugi</li>
<li>Bezmaksas dāvanu saiņošana</li>
<li>Bezmaksas apsveikumu kartīte</li>
<li>Bezmaksas piegāde no 39,95 EUR</li>
<li>Bezmaksas preces saņemšana t/c Alfa veikalos Douglas un Douglas Select</li>
</ul>
							</div>
						</div>					
					</div>
					<div class="clear"></div>
					<div id="footer_row2">
						<div class="copyright">DOUGLAS LATVIA SIA ©</div>
					</div>
				</div>				
				<div class="douglas_stamp"></div>				
			</div>
		</div>

	</div>

	<div id="ajax_loader"><img src="https://www.douglas.lv/images/frontend/ajax-loader.gif" alt="" /></div>
	
		
	
	<!-- Google Code for Remarketing Tag -->
	<!--------------------------------------------------
	Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 963068185;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963068185/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	<!-- Google Code for Remarketing Tag End -->
	<script src="<?php echo $baseUrl; ?>js/snow.js?v2"></script>
</body>
</html>